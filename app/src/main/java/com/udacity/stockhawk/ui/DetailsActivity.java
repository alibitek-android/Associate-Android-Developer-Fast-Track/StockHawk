package com.udacity.stockhawk.ui;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.opencsv.CSVReader;
import com.udacity.stockhawk.R;
import com.udacity.stockhawk.data.Contract;

import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsActivity extends AppCompatActivity {

    public static String SYMBOL_EXTRA = "SYMBOL_EXTRA";

    @BindView(R.id.tv_symbolName)
    TextView mSymbolTextView;

    @BindView(R.id.stockPriceChart)
    LineChart mStockPriceChart;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        if (intent != null)
        {
            if (intent.hasExtra(SYMBOL_EXTRA)) {
                String symbol= intent.getStringExtra(SYMBOL_EXTRA);
                if (symbol != null) {
                    Toast.makeText(this, symbol, Toast.LENGTH_SHORT).show();
                    mSymbolTextView.setText(symbol);
                    getHistory(symbol);
                }
            }
        }
    }

    private void getHistory(String symbol) {
        String history = getHistoryData(symbol);
        //mSymbolTextView.setText(history);

        List<Entry> entries = new ArrayList<>();

        List<String[]> lines = getLines(history);

        final List<Long> xAxisValues = new ArrayList<>();
        int xAxisPosition = 0;

        for (int i = lines.size() - 1; i >= 0; i--) {
            String[] line = lines.get(i);

            // setup xAxis
            xAxisValues.add(Long.valueOf(line[0]));
            xAxisPosition++;

            // add entry data
            Entry entry = new Entry(xAxisPosition, // timestamp
                                    Float.valueOf(line[1]) // price
            );
            entries.add(entry);
        }

        drawChart(symbol, entries, xAxisValues);
    }

    private void drawChart(String symbol, List<Entry> entries, final List<Long> xAxisValues) {
        Description description = new Description();
        description.setText("");
        mStockPriceChart.setDescription(description);

        LineDataSet dataSet = new LineDataSet(entries, symbol);
        dataSet.setColor(Color.RED);

        LineData lineData = new LineData(dataSet);
        mStockPriceChart.setData(lineData);

        XAxis xAxis = mStockPriceChart.getXAxis();
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                Date date = new Date(xAxisValues.get(xAxisValues.size() - (int) value - 1));
                return (new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(date));
            }
        });

        // refresh
        mStockPriceChart.invalidate();
    }

    private List<String[]> getLines(String history) {
        List<String[]> lines = null;
        CSVReader csvReader = new CSVReader(new StringReader(history));
        try {
             lines = csvReader.readAll();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    private String getHistoryData(String symbol) {
        Cursor cursor = getContentResolver().query(Contract.Quote.makeUriForStock(symbol),
                null,
                null,
                null,
                null);

        String history = "";
        if (cursor != null && cursor.moveToFirst()) {
            history = cursor.getString(cursor.getColumnIndex(Contract.Quote.COLUMN_HISTORY));
            cursor.close();
        }
        return history;
    }
}
